#!/bin/bash

# System presets to keep Redis happy
echo "Tuning kernel options for Redis"
sudo sysctl -w vm.overcommit_memory=1
sudo sysctl -w net.core.somaxconn=1024
sudo sysctl -w net.ipv4.tcp_max_syn_backlog=2048
sudo sh -c "echo never > /sys/kernel/mm/transparent_hugepage/enabled"
sudo systemctl restart docker


echo "Starting Redis Master"
cp master_redis.conf master/redis.conf
docker run --rm --name master -d -v `pwd`/master/redis.conf:/home/redis/redis.conf redis:latest
MASTER_IP=$(sudo docker inspect --format '{{ .NetworkSettings.IPAddress }}' master)
echo "Redis master on $MASTER_IP"

read -p "Press any key to continue"
# Prepare configuration file for first slave
cp slave_redis.conf slave1/redis.conf
sed -i "s/^\# slaveof.*/slaveof $MASTER_IP 6379/" slave1/redis.conf
echo "Starting Redis Slave1"
docker run --rm --name slave1 -d -v `pwd`/slave1/redis.conf:/home/redis/redis.conf redis:latest
SLAVE1_IP=$(sudo docker inspect --format '{{ .NetworkSettings.IPAddress }}' slave1)
echo "Redis Slave1 on $SLAVE1_IP"

read -p "Press any key to continue"
# Prepare the sentinels
for i in 1 2 3; do
	cp sentinel.conf sentinel$i/sentinel.conf
	sed -i "s/MASTER_IP/$MASTER_IP/g" sentinel$i/sentinel.conf
	echo "Starting Redis Sentinel$i"
	docker run --rm --name sentinel$i -d -e STARTOPS="/home/redis/redis.conf --sentinel" -v `pwd`/sentinel$i/sentinel.conf:/home/redis/redis.conf redis:latest
	echo "Redis Sentinel$i $(sudo docker inspect --format '{{ .NetworkSettings.IPAddress }}' sentinel$i)"
	read -p "Press any key to continue"
done

#DOCKER_IP=$(ifconfig docker0 |grep "inet "|awk -F ' ' '{print $2}')