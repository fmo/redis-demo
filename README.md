#Redis demo

==========

This creates a Redis Sentinel demo cluster using docker

To build the docker image run:
> docker build -t redis:latest -f dockerbuild/Dockerfile_Redis .

To start the Redis cluster:
> ./redis-demo.sh

